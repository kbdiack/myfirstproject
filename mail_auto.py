from __future__ import print_function
import time
import datetime
import sib_api_v3_sdk
from sib_api_v3_sdk.rest import ApiException
from pprint import pprint


def mail_rappel(data, jour):
    configuration = sib_api_v3_sdk.Configuration()
    configuration.api_key['api-key'] = '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'

    api_instance = sib_api_v3_sdk.TransactionalEmailsApi(sib_api_v3_sdk.ApiClient(configuration))
    subject = "Mail de Rappel de sécurité"

    html_content = "<html><body>N'oubliez pas votre mission de demain s'il vous plait. </body></html>"

    sender = {"name": "Réseau Médical service", "email": "rmsit2@reseaumedical.fr"}

    for id in data:
        if id["p_mission"] == str(jour):
            l = []
            l_ = {}
            l_["email"] = id["email"]
            l_["name"] = id["name"]
            l.append(l_)
            to = l
            print(l)

            headers = {"Some-Custom-Name": "unique-id-1234"}
            params = {"parameter": "My param value", "subject": "New Subject"}
            send_smtp_email = sib_api_v3_sdk.SendSmtpEmail(to=to, headers=headers, html_content=html_content,sender=sender, subject=subject)
            try:
                api_response = api_instance.send_transac_email(send_smtp_email)

                pprint(api_response)
            except ApiException as e:
                print("Exception when calling SMTPApi->send_transac_email: %s\n" % e)


def mail_diffusion(data, mission):
    configuration = sib_api_v3_sdk.Configuration()
    configuration.api_key['api-key'] = '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'

    api_instance = sib_api_v3_sdk.TransactionalEmailsApi(sib_api_v3_sdk.ApiClient(configuration))
    subject = "Mail de diffusion"

    html_content = "<html><body>Ceci est un mail de diffusion, il contient une mission pouvant vous intéresser. </body></html>"

    sender = {"name": "Réseau Médical service", "email": "rmsit2@reseaumedical.fr"}

    for id in data:
        if id["specialité"] == mission["specialité"] and id["niveau"] == mission["niveau"]:
            l = []
            l_ = {}
            l_["email"] = id["email"]
            l_["name"] = id["name"]
            l.append(l_)
            to = l
            print(l)

            headers = {"Some-Custom-Name": "unique-id-1234"}
            params = {"parameter": "My param value", "subject": "New Subject"}
            send_smtp_email = sib_api_v3_sdk.SendSmtpEmail(to=to, headers=headers, html_content=html_content, sender=sender, subject=subject)

            try:
                api_response = api_instance.send_transac_email(send_smtp_email)

                pprint(api_response)
            except ApiException as e:
                print("Exception when calling SMTPApi->send_transac_email: %s\n" % e)





###test des fonction
data=[{"email":"kenoushbd@gmail.com","name":"Mamy Diack","p_mission":"2022-11-21","specialité":"Pédiatrie", "niveau":"2A" },{"email":"kdiack96@gmail.com","name":"Kene Diack","p_mission":"2022-11-21","specialité":"Pédiatrie", "niveau":"2B" },{"email":"kenebougouldiack96@gmail.com","name":"Mamy Kene Diack","p_mission":"2022-11-22" ,"specialité":"Pédiatrie", "niveau":"1"}]
d=datetime.date(2022, 11, 21)
mission={"specialité":"Pédiatrie", "niveau":"2A"}
print(d)
it1=mail_rappel(data,d)
it2=mail_diffusion(data,mission)



